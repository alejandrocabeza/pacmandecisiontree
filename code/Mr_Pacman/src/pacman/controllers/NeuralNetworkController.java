package pacman.controllers;

import neuralNetwork.MultilayerPerceptron;
import neuralNetwork.NeuralNetworkImp;
import pacman.game.Constants;
import dataRecording.DataTuple;
import decisionTree.*;
import pacman.game.Constants.*;
import pacman.game.Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by daniel.sanchez on 08/01/2015.
 */
public class NeuralNetworkController extends Controller<Constants.MOVE>
{
        /**
         * Multilayer Perceptron to traain and ask for result
        */
        private MultilayerPerceptron mlp;

    /**
     * Dataset to be used for train the multilayer perceptron
     */
        private PacmanDataSetNW      pDataSet;

    /**
     * Neural network controller
     * @param datasetpath       path to the dataset file
     * @param multilayer2load   path to the file for loading with Gson DEPRECATED
     */
        public NeuralNetworkController(String datasetpath, String multilayer2load) {


            pDataSet = new PacmanDataSetNW(datasetpath);

            DataSetInt tmpdataset = pDataSet.getDataSet().getClassTypes().getTuple(0).removeAttributtes(pDataSet.getDataSet().getClassId());
            /*
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            Integer answer = 0;
            try {
                System.out.println("Press '1' for let the neural network be trained or '2' to load a prefab network: ");
                answer = Integer.parseInt(br.readLine());
            }
            catch (IOException e) {System.out.println("Sorry you misschoose....run  it again :P");}*/
            mlp = initMultilayerPerceptron(tmpdataset);
            trainit(mlp);
            /*
            switch (answer)
            {
                case 0:
                    System.out.println("It will be train.....");
                case 1:
                    trainit(mlp);
                    break;
                case 2:
                    mlp.loadLayers(multilayer2load);
                    break;
            }*/
        }

    /**
     * Initialize a multilayer perceptron
     * @param tmpdataset DataSet with the info needed to initialize the multilayer perceptron.
     * @return
     */
    public MultilayerPerceptron initMultilayerPerceptron(DataSetInt tmpdataset)
    {
        ArrayList<AttributeInt> input = new ArrayList<AttributeInt>();
        for (String id: tmpdataset.getAttributeNames())
        {
            input.add(tmpdataset.getValue(id,0));
        }

        ArrayList<Integer> hiddens = new ArrayList<Integer>();
        //hiddens.add(5);
        hiddens.add(10);
        //hiddens.add(10);
        //hiddens.add(20);

        ArrayList<AttributeInt> output = pDataSet.getDataSet().getClassTypes().getAttributeArray(pDataSet.getDataSet().getClassTypes().getClassId());
        MultilayerPerceptron mlp = new MultilayerPerceptron(input, hiddens, output);

        return mlp;
    }

    /**
     * Train the expected multilayer perceptron with the class dataset
     * @param mlp Multilayer Perceptron to be trained.
     */
    private void trainit(MultilayerPerceptron mlp)
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Integer answer1 = 50;
        Integer answer2 = 10000;
        try {
            System.out.println("Enter the % of accuracy: ");
            answer1 = Integer.parseInt(br.readLine());
            System.out.println("Enter the limit of iteration for training");
            answer2 = Integer.parseInt(br.readLine());
        }
        catch (IOException e) {System.out.println("Sorry you misschoose....run  it again :P");}
        mlp.train(pDataSet.getDataSet(), answer1, answer2);
    }
        /**
         * Calculates the move PacMan should make according to the DecisionTree built through the previously specified algorithm and DataSet
         * @param game A copy of the current game
         * @param timeDue The time the next move is due
         * @return Move Pacman should make
         */
        public MOVE getMove(Game game, long timeDue)
        {
            //Creates a 1 tuple Dataset
            //This is done to preserve the use in types throughout the practice
            DataSetImp gameData = new DataSetImp(PacmanDataSetNW.getAttributes());
            DataTuple tuple = new DataTuple(game, MOVE.NEUTRAL);

            //The game state is added to the DataSet
            //gameData.putValue(new AttributeImp<Float> ("pacmanPosition",      (float)tuple.normalizePosition(tuple.pacmanPosition)));
            gameData.putValue(new AttributeImp<Float> ("numOfPowerPillsLeft", (float)tuple.normalizeNumberOfPowerPills(tuple.numOfPowerPillsLeft)));
            gameData.putValue(new AttributeImp<Float> ("isBlinkyEdible",      (float)tuple.normalizeBoolean(tuple.isBlinkyEdible)));
            gameData.putValue(new AttributeImp<Float> ("isInkyEdible",        (float)tuple.normalizeBoolean(tuple.isInkyEdible)));
            gameData.putValue(new AttributeImp<Float> ("isPinkyEdible",       (float)tuple.normalizeBoolean(tuple.isPinkyEdible)));
            gameData.putValue(new AttributeImp<Float> ("isSueEdible",         (float)tuple.normalizeBoolean(tuple.isSueEdible)));
            gameData.putValue(new AttributeImp<Float> ("blinkyDist",          (float)tuple.normalizeDistance(tuple.blinkyDist)));
            gameData.putValue(new AttributeImp<Float> ("inkyDist",            (float)tuple.normalizeDistance(tuple.inkyDist)));
            gameData.putValue(new AttributeImp<Float> ("pinkyDist",           (float)tuple.normalizeDistance(tuple.pinkyDist)));
            gameData.putValue(new AttributeImp<Float> ("sueDist",             (float)tuple.normalizeDistance(tuple.sueDist)));
            gameData.putValue(new AttributeImp<Float> ("blinkyDir",           (float)tuple.blinkyDir.ordinal()/4));
            gameData.putValue(new AttributeImp<Float> ("inkyDir",             (float)tuple.inkyDir.ordinal()/4));
            gameData.putValue(new AttributeImp<Float> ("pinkyDir",            (float)tuple.pinkyDir.ordinal()/4));
            gameData.putValue(new AttributeImp<Float> ("sueDir",              (float)tuple.sueDir.ordinal()/4));

            //Returns the move Pacman should make according to the game state and the decision tree built
            MOVE toRet = (MOVE)mlp.getResult(gameData).getValue();
            if (toRet == null)
            {System.out.println("Null");return MOVE.RIGHT;}
            return toRet;
        }
}
