package pacman.controllers;

import dataRecording.DataTuple;
import decisionTree.*;
import pacman.game.Constants.*;
import pacman.game.Game;

/**
 * Custom controller that uses a Deicision Tree built with the ID3 or C4.5 algorithm
 * Created by Netwave on 29/11/2014.
 */
public class DecisionTreeController extends Controller<MOVE>
{
    private DecisionTree    decisionTree;
    private PacmanDataSet   pDataSet;

    public DecisionTreeController(AttributeSelectors.SELECTORS algorithm, String path) {
        decisionTree    = new DecisionTree(algorithm);
        pDataSet        = new PacmanDataSet(path);
        decisionTree.build(pDataSet.getDataSet());
    }

    /**
     * Calculates the move PacMan should make according to the DecisionTree built through the previously specified algorithm and DataSet
     * @param game A copy of the current game
     * @param timeDue The time the next move is due
     * @return Move Pacman should make
     */
    public MOVE getMove(Game game, long timeDue)
    {
        //Creates a 1 tuple Dataset
        //This is done to preserve the use in types throughout the practice
        DataSetImp gameData = new DataSetImp(PacmanDataSet.getAttributes());
        DataTuple tuple = new DataTuple(game, MOVE.NEUTRAL);

        //The game state is added to the DataSet
        //gameData.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("pacmanPosition", tuple.discretizePosition(tuple.pacmanPosition)));
        gameData.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("numOfPowerPillsLeft", tuple.discretizeNumberOfPowerPills(tuple.numOfPowerPillsLeft)));
        gameData.putValue(new AttributeImp<Double>                   ("isBlinkyEdible", tuple.normalizeBoolean(tuple.isBlinkyEdible)));
        gameData.putValue(new AttributeImp<Double>                   ("isInkyEdible", tuple.normalizeBoolean(tuple.isInkyEdible)));
        gameData.putValue(new AttributeImp<Double>                   ("isPinkyEdible", tuple.normalizeBoolean(tuple.isPinkyEdible)));
        gameData.putValue(new AttributeImp<Double>                   ("isSueEdible", tuple.normalizeBoolean(tuple.isSueEdible)));
        gameData.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("blinkyDist", tuple.discretizeDistance(tuple.blinkyDist)));
        gameData.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("inkyDist", tuple.discretizeDistance(tuple.inkyDist)));
        gameData.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("pinkyDist", tuple.discretizeDistance(tuple.pinkyDist)));
        gameData.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("sueDist", tuple.discretizeDistance(tuple.sueDist)));
        gameData.putValue(new AttributeImp<MOVE>           ("blinkyDir", tuple.blinkyDir));
        gameData.putValue(new AttributeImp<MOVE>           ("inkyDir", tuple.inkyDir));
        gameData.putValue(new AttributeImp<MOVE>           ("pinkyDir", tuple.pinkyDir));
        gameData.putValue(new AttributeImp<MOVE>           ("sueDir", tuple.sueDir));
        gameData.putValue(new AttributeImp<MOVE>           ("DirectionChosen", tuple.DirectionChosen));

        //Returns the move Pacman should make according to the game state and the decision tree built
        return (MOVE)decisionTree.ask(gameData).getValue();
    }
}
