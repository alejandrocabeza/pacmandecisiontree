package decisionTree;

import dataRecording.DataTuple;
import pacman.game.Constants;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Initializes the PacmanDataSet class, creating a HashMap where the keys are the attributes of the DataSet and the values are ArrayLists that contain all the tuples values of the attribute
 * The class Attribute will be assigned to the last Attribute added to the ArrayList in the method getAttributes()
 * The DataSet is initialized reading the data from the .txt file in path specified below (file)
 * There needs to be a call to putValue(AttributeImp) for each attribute
 * Created by Netwave on 26/11/2014.
 */
public class PacmanDataSet
{
    protected DataSetImp dataSet;

    public PacmanDataSet(String path)
    {
        dataSet = new DataSetImp(getAttributes());

        //Reads line by line all the file with the path specified below;
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line;

            //Exits the program if the dataset is empty
            if (!br.ready())
            {
                System.out.println(path + " is emtpy.");
                System.exit(1);
            }

            while ((line = br.readLine()) != null)
            {
                //A line read from the file is converted into a tuple.
                //Value are discretized and converted to strings for them to be stored into the Dataset Attributes ArrayLists
                DataTuple tuple = new DataTuple(line);

                parseTuple(tuple);
            }
            br.close();
        }
        catch(IOException ex)
        {
            System.out.println (ex.toString());
            System.out.println("Could not find file " + path);
        }
    }

    public DataSetImp getDataSet() {
        return dataSet;
    }

    /**
     * Returns an ArrayList containing the attributes of the DataSet
     * @return An ArrayList containing the attributes of the DataSet
     */
    public static ArrayList<String> getAttributes()
    {
        ArrayList<String> atts = new ArrayList<String>();

        //atts.add("pacmanPosition");
        atts.add("numOfPowerPillsLeft");
        atts.add("isBlinkyEdible");
        atts.add("isInkyEdible");
        atts.add("isPinkyEdible");
        atts.add("isSueEdible");
        atts.add("blinkyDist");
        atts.add("inkyDist");
        atts.add("pinkyDist");
        atts.add("sueDist");
        atts.add("blinkyDir");
        atts.add("inkyDir");
        atts.add("pinkyDir");
        atts.add("sueDir");
        atts.add("DirectionChosen");

        return atts;
    }

    /**
     * Parses a tuple adding the necessary attributes to the dataset
     * @param tuple Tuple to be parsed
     */
    public void parseTuple (DataTuple tuple)
    {
        //dataSet.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("pacmanPosition",      tuple.discretizePosition(tuple.pacmanPosition)));
        dataSet.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("numOfPowerPillsLeft", tuple.discretizeNumberOfPowerPills(tuple.numOfPowerPillsLeft)));
        dataSet.putValue(new AttributeImp<Double>                   ("isBlinkyEdible",      tuple.normalizeBoolean(tuple.isBlinkyEdible)));
        dataSet.putValue(new AttributeImp<Double>                   ("isInkyEdible",        tuple.normalizeBoolean(tuple.isInkyEdible)));
        dataSet.putValue(new AttributeImp<Double>                   ("isPinkyEdible",       tuple.normalizeBoolean(tuple.isPinkyEdible)));
        dataSet.putValue(new AttributeImp<Double>                   ("isSueEdible",         tuple.normalizeBoolean(tuple.isSueEdible)));
        dataSet.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("blinkyDist",          tuple.discretizeDistance(tuple.blinkyDist)));
        dataSet.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("inkyDist",            tuple.discretizeDistance(tuple.inkyDist)));
        dataSet.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("pinkyDist",           tuple.discretizeDistance(tuple.pinkyDist)));
        dataSet.putValue(new AttributeImp<DataTuple.DiscreteTag>    ("sueDist",             tuple.discretizeDistance(tuple.sueDist)));
        dataSet.putValue(new AttributeImp<Constants.MOVE>           ("blinkyDir",           tuple.blinkyDir));
        dataSet.putValue(new AttributeImp<Constants.MOVE>           ("inkyDir",             tuple.inkyDir));
        dataSet.putValue(new AttributeImp<Constants.MOVE>           ("pinkyDir",            tuple.pinkyDir));
        dataSet.putValue(new AttributeImp<Constants.MOVE>           ("sueDir",              tuple.sueDir));
        dataSet.putValue(new AttributeImp<Constants.MOVE>           ("DirectionChosen",     tuple.DirectionChosen));
    }


    public static void main(String[] args)
    {
    /*
        PacmanDataSet Pdataset = new PacmanDataSet ();

        DataSetImp dataset = Pdataset.getDataSet().filter(new AttributeImp("pacmanPosition", DataTuple.DiscreteTag.VERY_LOW));
        //Tuple Printer Test
        int size = dataset.getAttributeArray("pacmanPosition").size();
        for (int i = 0; i < size; ++i)
        {
            System.out.print(dataset.getAttributeArray("pacmanPosition").get(i).getValue());
            System.out.print(", ");
            System.out.print(dataset.getAttributeArray("DirectionChosen").get(i).getValue());
            System.out.print(", ");
            System.out.print(dataset.getAttributeArray("numOfPowerPillsLeft").get(i).getValue());
            System.out.print("\n");
        }
        for(Pair<AttributeInt, Integer> p : Pdataset.getDataSet().getUniquesCount("pacmanPosition"))
        {
            System.out.println(p.fst.toString() + " : " + p.snd.toString());
        }
        System.out.println(size);
        */
    }
}
