package decisionTree;

import java.util.ArrayList;

/**
 * Contains the necessary functions to generate Decision Trees
 * Created by Netwave on 24/11/2014.
 */
public class AttributeSelectors
{
    /**
     * Enum containing the types of attribute selecting algorithms
     */
    public enum SELECTORS
    {
        ID3,
        C45,
        CART
    }

    private SELECTORS selector;

    public AttributeSelectors(SELECTORS sel)
    {
        selector = sel;
    }

    public String select(DataSetInt dataset)
    {
        String ret = "";
        switch (selector)
        {
            case ID3:   ret = id3  (dataset); break;
            case C45:   ret = c45  (dataset); break;
            case CART:  ret = cart (dataset); break;
        }
        return ret;
    }

    /**
     * Decision Tree Att Selector using the ID3 algorithm
     * @param dataset Discretized dataset from where the decision tree is generated
     * @return The name of the best attribute to classify the dataset
     */
    static public String id3 (DataSetInt dataset)
    {
        String bestAttribute = dataset.getAttributeNames().get(0);
        ArrayList<String> attNames = dataset.getAttributeNames();
        ArrayList<AttributeInt> classAtt = dataset.getAttributeArray(attNames.get(attNames.size() - 1));

        //Checking which attribute has the highest gain
        for (int i = 1; i < attNames.size() - 1; ++i)
        {
            //If the checked attribute has a higher gain than the best attribute, the checked attribute is set as best attribute
            if (gain(dataset.getAttributeArray(attNames.get(i)), classAtt) > gain(dataset.getAttributeArray(bestAttribute), classAtt))
            {
                bestAttribute = attNames.get(i);
            }
        }
        return bestAttribute;
    }

    /**
     * Decision Tree Att Selector using the C4.5 algorithm
     * @param dataset Discretized dataset from where the decision tree is generated
     * @return The name of the best attribute to classify the dataset
     */
    static public String c45 (DataSetInt dataset)
    {
        String bestAttribute = dataset.getAttributeNames().get(0);
        ArrayList<String> attNames = dataset.getAttributeNames();
        ArrayList<AttributeInt> classAtt = dataset.getAttributeArray(attNames.get(attNames.size() - 1));

        //Checking which attribute has the highest gain
        for (int i = 1; i < attNames.size() - 1; ++i)
        {
            //If the checked attribute has a higher gain ratio than the best attribute, the checked attribute is set as best attribute
            if (gainRatio(dataset.getAttributeArray(attNames.get(i)), classAtt) > gainRatio(dataset.getAttributeArray(bestAttribute), classAtt))
            {
                bestAttribute = attNames.get(i);
            }
        }
        return bestAttribute;
    }

    /**
     * Decision Tree Att Selector using the CART algorithm
     * @param dataset Discretized dataset from where the decision tree is generated
     * @return The name of the best attribute to classify the dataset
     */
    static public String cart (DataSetInt dataset)
    {
        //ToDo
        return "";
    }

    /**
     * Calculates the info of the class of the DataSet
     * @param classAttr The class ArrayList of the Dataset
     * @return The info of the class
     */
    static private float infoClass (ArrayList<AttributeInt> classAttr)
    {
        ArrayList<AttributeInt>  uniques = getUniques(classAttr);
        ArrayList<Integer> uniquesValues = getUniquesValues(classAttr, uniques);

        float info = 0;
        float prob = 0.f;

        for (int i = 0; i < uniques.size(); ++i)
        {
            prob = uniquesValues.get(i) / classAttr.size();
            info += (-prob*binaryLog(prob));
        }

        return info;
    }

    /**
     * Calculates the info of an attribute of the dataset other than the class
     * @param classAttr The attribute ArrayList of the Dataset
     * @return The info of the attribute
     */
    static private float infoAttr (ArrayList<AttributeInt> attrs, ArrayList<AttributeInt> classAttr)
    {
        ArrayList<AttributeInt>  uniques = getUniques(attrs);
        ArrayList<Integer> uniquesValues = getUniquesValues(attrs, uniques);

        ArrayList<AttributeInt>  uniquesClass = getUniques(classAttr);

        float info = 0;

        for (int i = 0; i < uniques.size(); ++i)
        {
            float inParentheses = 0.f;

            for (int j = 0; j < uniquesClass.size(); ++j)
            {
                float count = 0.f;

                for (int k = 0; k < classAttr.size(); ++k)
                {
                    if (classAttr.get(k).getValue().equals(uniquesClass.get(j).getValue())) ++count;
                }

                float fraction = count / uniquesValues.get(i);
                inParentheses += (-fraction*binaryLog(fraction));
            }

            info += (uniquesValues.get(i) / attrs.size()) * inParentheses;
        }

        return info;
    }

    /**
     * Calculates the gain in information of choosing the attribute attrs to classify the DataSet
     * @param attr The attribute ArrayList to calculate the gain
     * @param classAttr The class ArrayList of the DataSet
     * @return The information gain of choosing the attrs attribute
     */
    static private float gain (ArrayList<AttributeInt> attr, ArrayList<AttributeInt> classAttr)
    {
        return infoClass(classAttr) - infoAttr(attr, classAttr);
    }

    /**
     * Calculates the gain ratio in information of choosing the attribute attrs to classify the DataSet
     * @param attr The attribute ArrayList to calculate the gain ratio
     * @param classAttr The class ArrayList of the DataSet
     * @return The information gain ratio of choosing the attrs attribute
     */
    static private float gainRatio (ArrayList<AttributeInt> attr, ArrayList<AttributeInt> classAttr)
    {
        return (gain(attr, classAttr) / splitInfo(attr));
    }

    /**
     * Calculates the split information used to normalize the gain of an attribute
     * @param attr The attribute ArrayList to calculate the split information
     * @return The split information of the Attribute attr
     */
    static private float splitInfo (ArrayList<AttributeInt> attr)
    {
        ArrayList<AttributeInt>  uniques = getUniques(attr);
        ArrayList<Integer> uniquesValues = getUniquesValues(attr, uniques);

        float splitInfo = 0.f;
        float fraction  = 0.f;

        for (int i = 0; i < uniques.size(); ++i)
        {
            fraction = uniquesValues.get(i) / attr.size();
            splitInfo += (-fraction*binaryLog(fraction));
        }

        return splitInfo;
    }

    static private float gini ()
    {
        //ToDo
        return 0.f;
    }

    /**
     * Goes over an ArrayList of AttributeInt to get the values that appear at least one time
     * @param list AttributeInt ArrayList from which are to be extracted the unique values
     * @return An ArrayList with the unique values of the list
     */
    static private ArrayList<AttributeInt> getUniques (ArrayList<AttributeInt> list)
    {

        ArrayList<AttributeInt> uniques = new ArrayList<AttributeInt>();
        if(list != null)
        {
            //Gets the unique values from attrs
            for (int i = 0; i < list.size(); ++i) {
                Boolean add = true;

                for (int j = 0; j < uniques.size(); ++j) {
                    if (list.get(i).getValue().equals(uniques.get(j).getValue())) {
                        add = false;
                        break;
                    }
                }
                if (add) uniques.add(list.get(i));
            }
        }
        return uniques;
    }

    /**
     * Calculates the number of ocurrences of the different values in an ArrayLst
     * @param list An ArrayList which contains the elements to be counted
     * @param uniques An ArrayList which contains the unique elements to be counted
     * @return An ArrayList containing the number of ocurrences of each unique element in list. The position of the value in uniqueValues corresponds to the item in uniques
     */
    static private ArrayList<Integer> getUniquesValues (ArrayList<AttributeInt> list, ArrayList<AttributeInt> uniques)
    {
        ArrayList<Integer> uniqueValues = new ArrayList<Integer>();
        if(list != null) {
            //Adds uniques.size() values into the uniqueValues ArrayList to count the times a unique value is repeated
            for (int i = 0; i < uniques.size(); ++i) uniqueValues.add(0);

            //Counts the times an unique value appears in the attrs ArrayList
            for (int i = 0; i < list.size(); ++i) {
                for (int j = 0; j < uniques.size(); ++j) {
                    if (list.get(i).getValue().equals(uniques.get(j).getValue())) {
                        uniqueValues.set(j, uniqueValues.get(j) + 1);
                        break;
                    }
                }
            }
        }
        return uniqueValues;
    }

    /**
     * Calculates the binary logarithm of x
     * @param x The float to calculate the binary logarithm
     * @return The Binary Logarithm of x
     */
    static private float binaryLog(float x)
    {
        return (float)Math.log(x)/(float)Math.log(2);
    }

    public static void main(String[] args)
    {
        /*
        PacmanDataSet pdata = new PacmanDataSet();
        DataSetImp data = pdata.getDataSet();
        while(data.size() > 1)
        {
            String tag = c45(data);
            System.out.println(tag);
            data = data.removeAttributtes(tag);
        }*/
    }
}
