package decisionTree;

/**
 * Stores a pair of variables of any type
 * Created by Netwave on 29/11/2014.
 */
public class Pair<A,B> {
   /**
     * Generic Class A public access
     */
    public  A fst;

    /**
     * Generic Class B public access
     */
    public  B snd;

    public Pair(A a, B b)
    {
        fst = a;
        snd = b;
    }
}
