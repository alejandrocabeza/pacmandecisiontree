package decisionTree;

/**
 * DEPRECATED for this implementation
 * Types of Discretized Values to be used in the DataSet
 * In case of needing a new Discretized Value it should be added here
 * Created by Netwave on 24/11/2014.
 */
public enum AttType
{
    INT,
    DOUBLE,
    DISCRETETAG,
    MOVE,
    BOOL,
    NULL,
}
