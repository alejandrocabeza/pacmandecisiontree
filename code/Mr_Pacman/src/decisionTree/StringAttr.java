package decisionTree;

/**
 * AttributeInt that uses Strings as value
 * Not used in this implementation
 * Created by Netwave on 26/11/2014.
 */
public class StringAttr extends AttributeImp<String> {

    public StringAttr(String id, String val) {
        super(id, val);
    }
}
