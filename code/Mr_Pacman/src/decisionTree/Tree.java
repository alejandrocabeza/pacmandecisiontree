package decisionTree;

import java.util.ArrayList;

/**
 * Basic Tree that is able to add and remove nodes.
 * Created by Netwave on 29/11/2014.
 */
public class Tree<Data>
{
    protected Data            root;
    protected ArrayList<Tree> childs;
    protected boolean         isloaded;

    public Tree(Data root, ArrayList<Tree> childs)
    {
        this.root   = root;
        this.childs = childs;
        isloaded    = true;
    }

    public Tree(Data root)
    {
        this.root = root;
        childs = new ArrayList<Tree>();
        isloaded = true;
    }

    public Tree()
    {
        root = null;
        childs = new ArrayList<Tree>();
        isloaded = false;
    }

    /**
     * Checks if the tree is built
     * @return True if built, false if it isn't
     */
    public boolean          isLoaded    ()              {return isloaded;}

    /**
     * Gets the array of children from the current tree
     * @return ArrayList<Tree> of children from the current tree
     */
    public ArrayList<Tree> getChildren()              {return childs;}

    /**
     * Number of children from the current tree
     * @return Number of children of the current tree
     */
    public int             countChildren()              {return childs.size();}

    /**
     * Gets the data stored in the root
     * @return Generic data stored in the root
     */
    public Data             getData     ()              {return root;}

    /**
     * Checks if the current node is leaf
     * @return True if the node is a leaf, false if it isn't
     */
    public boolean          isLeaf      ()              {return countChildren()==0;}

    /**
     * Adds a child to the tree
     * @param child Tree to be added as child
     */
    public void             addChild(Tree child)    {childs.add(child);}

    /**
     * Set the root data
     * @param data Data to be added as root
     */
    public void             setData     (Data data)     {root = data;}

    /**
     * Prints the tree
     * @param numOfTabs First tab offset level to be printed (default is 0)
     */
    public void             print       (int numOfTabs)
    {
        for(int i = 0; i < numOfTabs; i++) System.out.print('\t');
        System.out.println(root.toString() + "->" + numOfTabs);
        for (Tree t: childs){t.print(numOfTabs+1);}
    }
}
