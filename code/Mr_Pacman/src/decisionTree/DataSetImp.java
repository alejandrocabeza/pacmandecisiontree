package decisionTree;

import  java.util.HashMap;
import  java.util.ArrayList;

/**
 * DataSetImp is HashMap containing Strings as key and ArrayLists of AttributeInt as value
 * Implements the interface DataSetInt
 * Check DataSetInt for method documentation.
 * Created by Netwave on 26/11/2014.
 */
public class DataSetImp extends HashMap<String, ArrayList<AttributeInt>> implements DataSetInt
{
    /**
     * Name of the attributes
     * The last attribute is the class attribute
     */
    protected ArrayList<String>                         attributeNames;

    /**
     * Attribute name of the class
     */
    protected String                                    classId;

    /**
     * Hash map containing list of all possible attributes and values for this Data set.
     */
    protected  DataSetInt                               classTypes = null;


    public DataSetImp(ArrayList<String> attrIds)
    {
        for(String id : attrIds)
        {
            put(id, new ArrayList<AttributeInt>());
        }
        attributeNames = attrIds;
        classId = attributeNames.get(attributeNames.size()-1);
    }

    @Override
    public ArrayList<String> getAttributeNames() {
        return attributeNames;
    }



    @Override
    public ArrayList<AttributeInt>  getAttributeArray(String id) {
        return get(id);
    }

    @Override
    public DataSetImp               removeAttributtes(String id)
    {
        DataSetImp retdataSetRet = (DataSetImp)clone();
        retdataSetRet.remove(id);
        retdataSetRet.attributeNames.remove(id);
        return retdataSetRet;
    }

    @Override
    public void                     addAttributes(String id, ArrayList<AttributeInt> data) {
        put(id, data);
    }

    @Override
    public AttributeInt             getValue(String id, int pos) {
        return get(id).get(pos);
    }

    @Override
    public void                     putValue(AttributeInt attr) {
        get(attr.getId()).add(attr);
    }

    @Override
    public DataSetImp               filter(AttributeInt attr)
    {
        DataSetImp ret =  new DataSetImp(new ArrayList<String>(attributeNames));
        for (int i = 0; i < get(attr.getId()).size(); i++)
        {
            if (get(attr.getId()).get(i).getValue().equals(attr.getValue()))
            {
                for (String key : keySet())
                {
                    ret.putValue(get(key).get(i));
                }
            }
        }
        return ret;
    }

    @Override
    public String getClassId() {return classId;}

    @Override
    public DataSetInt getClassTypes()
    {
        return classTypes;
    }

    @Override
    public DataSetInt getTuple(int tuple)
    {
        ArrayList<String> attrNames = getAttributeNames();
        DataSetImp retTuple = new DataSetImp(attrNames);
        for (String id : getAttributeNames())
        {
            retTuple.putValue(getValue(id, tuple));
        }
        return retTuple;
    }

    @Override
    public void addClassTypes(DataSetInt dataset)
    {
        classTypes = dataset;
    }

    @Override
    public ArrayList<AttributeInt> getUniques(String id)
    {
        ArrayList<AttributeInt> list    = getAttributeArray(id);
        ArrayList<AttributeInt> uniques = new ArrayList<AttributeInt>();
        //Gets the unique values from attrs
        if(list != null)
        {
            for (int i = 0; i < list.size(); ++i) {
                Boolean add = true;
                for (int j = 0; j < uniques.size(); ++j) {
                    if (list.get(i).getValue().equals(uniques.get(j).getValue())) {
                        add = false;
                        break;
                    }
                }
                if (add) uniques.add(list.get(i));
            }
        }
        return uniques;
    }

    @Override
    public ArrayList<Pair<AttributeInt, Integer>> getUniquesCount(String id) {
        ArrayList<AttributeInt> list                    = getAttributeArray(id);
        ArrayList<Pair<AttributeInt, Integer>> uniques  = new ArrayList<Pair<AttributeInt, Integer>>();
        //Gets the unique values from attrs
        for (int i = 0; i < list.size(); ++i)
        {
            Boolean add = true;
            for (int j = 0; j < uniques.size(); ++j)
            {
                if (list.get(i).getValue().equals(uniques.get(j).fst.getValue()))
                {
                    add = false;
                    uniques.get(j).snd += 1;
                    break;
                }
            }
            if (add) uniques.add(new Pair<AttributeInt, Integer>(list.get(i), 1));
        }
        return uniques;
    }
}
