package decisionTree;

import java.util.ArrayList;

/**
 * DataSetImp is HashMap containing Strings as key and ArrayLists of AttributeInt as value
 * Created by Netwave on 26/11/2014.
 */
public interface DataSetInt
{
    /**
     * Get a list with all the ids form the attributes on the data set
     * @return ArrayList<String> containing the ids
     */
    ArrayList<String>           getAttributeNames ();

    /**
     * Get a list with all the attributes whose ids are like the one in the parameter
     * @param id id of the attributes to be getted
     * @return ArrayList<AttributeInt> containing the attributes whose ids are like the one in the parameter
     */
    ArrayList<AttributeInt>     getAttributeArray (String id);

    /**
     * Remove all the attributes whose ids are like the one in the parameter
     * @param id id for filtering the attributes
     * @return A dataset containing the same data except the attributes whose ids are like the one in the parametter
     */
    DataSetInt                  removeAttributtes (String id);

    /**
     * Add a list of attributes to the dataset
     * @param id id of the attributes to be added
     * @param data list of attributes to be added
     */
    void                        addAttributes     (String id, ArrayList<AttributeInt> data);

    /**
     * get an attribute from the dataset
     * @param id id of the attribute to be get
     * @param pos position in the column where the attribute is located
     * @return an attribute from the dataset
     */
    AttributeInt                getValue          (String id, int pos);

    /**
     * put an attribute in the dataset
     * @param attr attribute to be added
     */
    void                        putValue          (AttributeInt attr);

    /**
     * filter the dataset, get a dataset with the tuples whose attribute/s are like the one in the parameter
     * @param attr attribute to be compared with
     * @return a dataset with the tuples whose attribute/s are like the one in the parameter
     */
    DataSetInt                  filter            (AttributeInt attr);

    /**
     * get the class id from the dataset
     * @return an String containing the class id from the dataset
     */
    String                      getClassId        ();

    /**
     * @return A new datasetint containing all the class types combinations
     */
    DataSetInt                  getClassTypes        ();

    /**
     * Get tuple aas Dataset int with size one
     * @param tuple
     * @return
     */
    DataSetInt                  getTuple            (int tuple);

    void                        addClassTypes       (DataSetInt dataset);

    /**
     * Get an array with attributes containing all the possible values that appear for an id in the dataset
     * @param id id to be checked
     * @return an array with attributes containing all the possible values that appear for an id in the dataset
     */
    ArrayList<AttributeInt>     getUniques        (String id);

    /**
     * Get an array with pairs of the unique values for an asked id and their count appearance in the dataset
     * @param id id to be checked
     * @return ArrayList<Pair <AttributeInt,Integer>> an array with pairs of the unique values for an asked id and their count appearance in the dataset
     */
    ArrayList<Pair <AttributeInt,
                    Integer>>   getUniquesCount   (String id);
}
