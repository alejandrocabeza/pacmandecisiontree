package decisionTree;

import dataRecording.DataTuple;
import pacman.game.Constants.MOVE;

import java.util.ArrayList;

/**
 * Initializes a PacmanDataSet that works with Neuronal Networks
 * Created by Strinnityk on 07/01/2015.
 */
public class PacmanDataSetNW extends PacmanDataSet
{
    public PacmanDataSetNW(String path)
    {
        super(path);
        dataSet.addClassTypes(getAuxDataSet());
    }

    /**
     * Creates an auxiliary dataset with all the attributes and an example value per each type the attribute can become
     * @return Returns the auxiliary dataset
     */
    public DataSetImp getAuxDataSet()
    {
        ArrayList<String> atts = getAttributes();
        DataSetImp auxDataSet = new DataSetImp(atts);

        //auxDataSet.putValue(new AttributeImp<Float> ("pacmanPosition",      0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("numOfPowerPillsLeft", 0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("isBlinkyEdible",      0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("isInkyEdible",        0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("isPinkyEdible",       0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("isSueEdible",         0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("blinkyDist",          0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("inkyDist",            0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("pinkyDist",           0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("sueDist",             0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("blinkyDir",           0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("inkyDir",             0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("pinkyDir",            0.0f));
        auxDataSet.putValue(new AttributeImp<Float> ("sueDir",              0.0f));
        auxDataSet.putValue(new AttributeImp<MOVE>  ("DirectionChosen",     MOVE.UP));
        auxDataSet.putValue(new AttributeImp<MOVE>  ("DirectionChosen", 	MOVE.DOWN));
        auxDataSet.putValue(new AttributeImp<MOVE>  ("DirectionChosen", 	MOVE.LEFT));
        auxDataSet.putValue(new AttributeImp<MOVE>  ("DirectionChosen", 	MOVE.RIGHT));
        auxDataSet.putValue(new AttributeImp<MOVE>  ("DirectionChosen", 	MOVE.NEUTRAL));

        return auxDataSet;
    }

    @Override
    public void parseTuple (DataTuple tuple)
    {
        //dataSet.putValue(new AttributeImp<Float> ("pacmanPosition",      (float)tuple.normalizePosition(tuple.pacmanPosition)));
        dataSet.putValue(new AttributeImp<Float> ("numOfPowerPillsLeft", (float)tuple.normalizeNumberOfPowerPills(tuple.numOfPowerPillsLeft)));
        dataSet.putValue(new AttributeImp<Float> ("isBlinkyEdible",      (float)tuple.normalizeBoolean(tuple.isBlinkyEdible)));
        dataSet.putValue(new AttributeImp<Float> ("isInkyEdible",        (float)tuple.normalizeBoolean(tuple.isInkyEdible)));
        dataSet.putValue(new AttributeImp<Float> ("isPinkyEdible",       (float)tuple.normalizeBoolean(tuple.isPinkyEdible)));
        dataSet.putValue(new AttributeImp<Float> ("isSueEdible",         (float)tuple.normalizeBoolean(tuple.isSueEdible)));
        dataSet.putValue(new AttributeImp<Float> ("blinkyDist",          (float)tuple.normalizeDistance(tuple.blinkyDist)));
        dataSet.putValue(new AttributeImp<Float> ("inkyDist",            (float)tuple.normalizeDistance(tuple.inkyDist)));
        dataSet.putValue(new AttributeImp<Float> ("pinkyDist",           (float)tuple.normalizeDistance(tuple.pinkyDist)));
        dataSet.putValue(new AttributeImp<Float> ("sueDist",             (float)tuple.normalizeDistance(tuple.sueDist)));
        dataSet.putValue(new AttributeImp<Float> ("blinkyDir",           (float)tuple.blinkyDir.ordinal()/4));
        dataSet.putValue(new AttributeImp<Float> ("inkyDir",             (float)tuple.inkyDir.ordinal()/4));
        dataSet.putValue(new AttributeImp<Float> ("pinkyDir",            (float)tuple.pinkyDir.ordinal()/4));
        dataSet.putValue(new AttributeImp<Float> ("sueDir",              (float)tuple.sueDir.ordinal()/4));
        dataSet.putValue(new AttributeImp<MOVE>  ("DirectionChosen",     tuple.DirectionChosen));
    }
}