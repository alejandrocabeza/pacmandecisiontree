package decisionTree;

/**
 * Generic interface to store two values, Id is a String and Value can be any
 * Created by Netwave on 24/11/2014.
 */
public interface AttributeInt<Type>
{
    /**
     * Attribute id getter
     * @return string containing the attribute id
     */
    public  String  getId       ();

    /**
     * Attribute GENERIC container getter
     * @return a Generic value from the container.
     */
    public  Type    getValue    ();
    //public  AttType getType     (); //Deprecated
}
