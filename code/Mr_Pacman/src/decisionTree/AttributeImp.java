package decisionTree;

/**
 * Attribute implementation class, Generic container for attributes, implements generic interface attribute
 * Created by Netwave on 24/11/2014.
 */
public class AttributeImp<Type> implements AttributeInt<Type>
{
    protected   String  m_id;
    protected   Type    m_value;
    //protected   AttType m_type; DEPRECATED

    public AttributeImp(String id, Type val/*, AttType type Deprecated*/)
    {
        m_id    = id;
        m_value = val;

        //Deprecated
        //m_type  = type;
    }
    public  String  getId       ()
    {
        return m_id;
    }
    public  Type    getValue    ()
    {
        return m_value;
    }

    /**
     * To string, represents the id of the attribute and the value from the ocntainer
     * @return an string representing the object id and container value.
     */
    @Override
    public String toString() {
        return "{" + m_id + " : " + m_value + "}";
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof AttributeImp)) return false;

        AttributeImp that = (AttributeImp) o;

        if (m_id != null ? !m_id.equals(that.m_id) : that.m_id != null) return false;
        if (m_value != null ? !m_value.equals(that.m_value) : that.m_value != null) return false;

        return true;
    }


    /*
    @Deprecated
    public  AttType getType     ()
    {
        //return m_type;
    }
    */
}
