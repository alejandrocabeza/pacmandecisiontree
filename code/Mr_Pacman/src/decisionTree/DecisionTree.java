package decisionTree;
import pacman.game.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Creates a DecisionTree that returns the best value according to a state
 */
public class DecisionTree extends Tree<AttributeInt>
{
	/**
	 * Attribute selector to be used
	 */
	private AttributeSelectors selector;

	/**
	 * Selector id to be used (ID3, C45, CART)
	 */
	private AttributeSelectors.SELECTORS selectorType;

	/**
	 * Implements a comparator of pairs of attribute interfaces and integers
	 */
	private class PairComparator implements Comparator<Pair<AttributeInt, Integer>>
	{
		/**
		 * Compares 2 pairs of values
		 * @param o1 Pair 1 to be compared
		 * @param o2 Pair 2 to be compared
		 * @return 0 if equals, positive integer if greater and negative integer if less.
		 */
		@Override
		public int compare(Pair<AttributeInt, Integer> o1, Pair<AttributeInt, Integer> o2) {
			return o1.snd.compareTo(o2.snd);
		}
	}

	public DecisionTree(AttributeSelectors.SELECTORS selectorType)
	{
		super();
		this.selectorType = selectorType;
		selector 		= new AttributeSelectors(selectorType);
	}

	/**
	 * Build the tree with the data from a dataset
	 * @param dataset DataSet to be used for building the tree
	 */
	public void build(DataSetInt dataset)
	{
		if		(dataset.getUniques(dataset.getClassId()).size() == 1)
		{
			//root = dataset.getValue(dataset.getClassId(),0);

			DecisionTree tree = new DecisionTree(selectorType);
			tree.setData(dataset.getValue(dataset.getClassId(),0));
			addChild(tree);
		}
		else if	(dataset.getAttributeNames().size() == 1)
		{
			//root = getBest(data);
			DecisionTree tree = new DecisionTree(selectorType);
			tree.setData(getBest(dataset));
			addChild(tree);
		}
		else
		{
			String tag = selector.select(dataset);
			if(root == null) root = dataset.getValue(tag,0);
			for (AttributeInt attr : dataset.getUniques(tag))
			{
				DecisionTree tree = new DecisionTree(selectorType);
				tree.setData(attr);
				tree.build(dataset.filter(attr).removeAttributtes(attr.getId()));
				addChild(tree);
			}
		}
		isloaded = true;
	}

	/**
	 * Entering point method that returns the best decision the tree is able to make according to the state
	 * @param tuple State to get the best value for
	 * @return An AttributeInt object with the best decision
	 */
	public AttributeInt ask(DataSetInt tuple)
	{
		return ask(tuple, this);
	}

	/**
	 * Overloaded function to make a recursive call to explore the nodes of the tree
	 * @param tuple DataSet interface object tuple to be used as data
	 * @param tree Tree to be used in the recursive call
	 * @return An AttributeInt object with the decision on the value ot it.
	 */
	private AttributeInt ask(DataSetInt tuple, Tree<AttributeInt> tree)
	{
		if(tree.isLeaf())
		{
			return tree.getData();
		}
		for (Tree<AttributeInt> t: tree.childs)
		{
			if (tuple.getValue(t.getData().getId(),0).getValue().equals(t.getData().getValue()))
			{
				return ask(tuple, t);
			}
		}
		for (Tree<AttributeInt> t: tree.childs)
		{
			if (t.isLeaf()) return t.getData();
		}
		return new AttributeImp<Constants.MOVE>("null", Constants.MOVE.UP);
	}

	/**
	 * Obtains the best decision from the class id from a dataset
	 * @param dataset Dataset for getting the information
	 * @return An attribute interface object with the decision on the value ot it.
	 */
	private AttributeInt getBest(DataSetInt dataset)
	{
		ArrayList<Pair<AttributeInt, Integer>> lst = dataset.getUniquesCount(dataset.getClassId());
		Collections.sort(lst, new PairComparator());
		return lst.get(lst.size()-1).fst;
	}

	/**
	 * Testing main
	 * @param args Not required and not used.
	 */
	public static void main(String args[])
	{
		/*
		PacmanDataSet pdata = new PacmanDataSet();
		DecisionTree t = new DecisionTree(AttributeSelectors.SELECTORS.C45);
		t.build(pdata.getDataSet());
		t.print(0);
		*/
	}
}
