package neuralNetwork;
import java.math.*;

/**
 * Contains static methods that calculate the activation value of neurons
 * Created by daniel.sanchez on 18/12/2014.
 */
public class LearningFactors
{
    /**
     * Enum with the TAGS of available learning algorithms
     */
    public enum Factors
    {
        SIGMOID
    }

    /**
     * Algorithm TAG to be used in this instance
     */
    Factors factor;

    /**
     * Learning Factor constructor
     * @param factor TAG of the algorithm to be used
     */
    public LearningFactors(Factors factor)
    {
        this.factor = factor;
    }

    /**
     * Generic method to calculate the values, uses a function according to the correspondant TAG
     * @param input Value to calculate with
     * @return Activation value
     */
    public float calculate(float input)
    {
        switch (factor)
        {
            case SIGMOID:   return sigmoid(input);
            default:        return 0.f;
        }
    }

    /**
     * Sigmoid algorithm
     * @param input Value to calculate with
     * @return The result of the sigmoid curve for the given value.
     */
    static private float sigmoid(float input)
    {
        return (float)(1/(1+(Math.exp(-input))));
    }
}
