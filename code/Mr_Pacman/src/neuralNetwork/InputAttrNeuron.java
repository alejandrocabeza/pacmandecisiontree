package neuralNetwork;
import decisionTree.AttributeImp;

import java.util.ArrayList;

/**
 * Implements the functionality a input neuron using a container of type AttributeImp<Float>
 * Created by Netwave on 21/12/2014.
 */
public class InputAttrNeuron extends InputNeuron<AttributeImp<Float>> {

    public InputAttrNeuron(String id, AttributeImp<Float> container) {
        super(id, container);
    }

    public float getActivation() {
        return container.getValue();
    }
}
