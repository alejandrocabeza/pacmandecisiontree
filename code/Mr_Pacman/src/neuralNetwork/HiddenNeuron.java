package neuralNetwork;

import java.util.ArrayList;

/**
 * Implements the functionality a hidden neuron uses
 * Created by daniel.sanchez on 09/12/2014.
 */
public class HiddenNeuron extends NeuronImp
{
    public HiddenNeuron(String id, ArrayList<Float> weights)
    {
        super(id, weights);
    }

    public HiddenNeuron(String id, int size)
    {
        super(id, size);
    }
}
