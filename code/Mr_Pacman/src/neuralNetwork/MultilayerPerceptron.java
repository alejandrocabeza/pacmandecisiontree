package neuralNetwork;

import decisionTree.AttributeInt;
import decisionTree.DataSetInt;
import decisionTree.PacmanDataSetNW;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Neural network that allows the containment of hidden layers
 * Created by daniel.sanchez on 16/12/2014.
 */
public class MultilayerPerceptron extends NeuralNetworkImp<AttributeInt>
{
    /**
     * Accuracy of the trained Multilayer Perceptron
     */
    public float accuracy = 0.f;

    /**
     * Multilayer Perceptron Constructor
     * @param inputdata         Generic Array list with the values for the input neurons to be initialized
     * @param hiddenLayersInfo  Array list with the number of hidden neurons to be initialized peer layer
     * @param outData           Generic Array list with the values for the output neurons to be initialized
     */
    public MultilayerPerceptron(ArrayList<AttributeInt> inputdata, ArrayList<Integer> hiddenLayersInfo, ArrayList<AttributeInt> outData)
    {
        super(inputdata, hiddenLayersInfo, outData, LearningFactors.Factors.SIGMOID);
        /*Deprecated, done in father class
        for (int i = 0; i < hiddenLayersInfo.size(); i++)
        {
            for (int j = 0; j < hiddenLayersInfo.get(i); j++)
            {
                hiddenNeuronsLayers.get(i).add(new HiddenNeuron());
            }
        }*/
    }

    @Override
    public void train(DataSetInt data, float expectedAccuracy, int maxIterations)
    {
        super.train(data, expectedAccuracy, maxIterations);
        int     iteration   = 0;
        int     counter     = 0;
        int     dataSetSize = data.getAttributeArray(data.getAttributeNames().get(0)).size();
        while (accuracy < expectedAccuracy && iteration < maxIterations)
        {
            //System.out.println(iteration);
            counter = 0;
            //if (iteration > 1000) {iteration = 100;System.out.println("Accuracy = " + accuracy + " / Counter = " + counter);}
            for (int i = 0; i < dataSetSize; i++)
            {
                trainOne(data.getTuple(i), iteration);
            }

            for (int i = 0; i < dataSetSize; i++)
            {
                DataSetInt      newData     =   data.getTuple(i);
                AttributeInt    res         =   getResult(newData);
                AttributeInt    tocheck     =   newData.getValue(data.getClassId(),0);

                if (res.equals(tocheck)) counter++;
            }

            iteration++;
            accuracy = ((float)counter/dataSetSize)*100;
            //System.out.println(toString());
        }
        accuracy = ((float)counter/dataSetSize)*100;
        System.out.println("Accuracy = " + accuracy + " / Counter = " + counter);
    }

    @Override
    protected void trainOne(DataSetInt data, int iteration) {
        super.trainOne(data, iteration);
        //Initialize input neurons with new data, a dataset with just one tuple
        initializeInputNeurons(data);
        ArrayList<Float> values = new ArrayList<Float>();
        for (NeuronGen<AttributeInt> in : inputNeurons)
        {
            values.add((Float)in.getContainer().getValue());
        }
        for (int i = 0; i < hiddenNeuronsLayers.size(); i++)
        {
            values = hiddenLayerActivation(i, values);
            //System.out.println(values.toString());
        }
        outputActivation(values);

        int pos = 0;
        AttributeInt    tocheck     =   data.getValue(data.getClassId(),0);
        OutputNeuron<AttributeInt> ret = null;
        for (OutputNeuron<AttributeInt> on : outputNeurons)
        {
            if (pos == 0) ret = on;
            else
            {
                if (ret.getActivation() < on.getActivation()) ret = on;
            }
            pos++;
        }
        if (!ret.equals(tocheck)) backpropagateError(data, iteration);
    }



    @Override
    protected void backpropagateError(DataSetInt data, float iterationValue)
    {
        super.backpropagateError(data, iterationValue);
        float learningFactor = 1/(iterationValue+1);
        ArrayList<Float> errors = new ArrayList<Float>();
        for (OutputNeuron<AttributeInt> on : outputNeurons)
        {
            float err = on.calculateError(data.getValue(data.getClassId(), 0));
            errors.add(err);
            on.update(learningFactor, err, hiddenLayerActivation(hiddenNeuronsLayers.size()-1));
        }
        for (int i = hiddenNeuronsLayers.size()-1; i >= 0; i--)
        {
            ArrayList<Float> errorsTmp = new ArrayList<Float>();
            for (int n = 0; n < hiddenNeuronsLayers.get(i).size(); n++)
            {
                float errorWeightsSumation = 0.f;
                for (int e = 0; e < errors.size(); e++)
                {
                    //Si no es la ultima capa de neuronas ocultas
                    if(i < hiddenNeuronsLayers.size()-1)
                    {
                        errorWeightsSumation += errors.get(e)* hiddenNeuronsLayers.get(i+1).get(e).getWeights().get(n);
                    }
                    else //si es la ultima capa de neuronas oculta (se enlaza con las de salida9
                    {
                        errorWeightsSumation += errors.get(e)*outputNeurons.get(e).getWeights().get(n);
                    }
                }
                float err = hiddenNeuronsLayers.get(i).get(n).calculateError(errorWeightsSumation);
                errorsTmp.add(err);
                if      (i >= 1)  hiddenNeuronsLayers.get(i).get(n).update(learningFactor , err, hiddenLayerActivation(i-1));
                else if (i  == 0)
                {
                    ArrayList<Float> inputActivation = new ArrayList<Float>();
                    for (NeuronGen<AttributeInt<Float>> in: inputNeurons ) inputActivation.add(in.getContainer().getValue());
                    hiddenNeuronsLayers.get(i).get(n).update(learningFactor , err, inputActivation);
                }
                else System.out.println("Error calculando propagate error en la ultima capa");

            }
            errors = errorsTmp;
        }
    }


    @Override
    public AttributeInt getResult(DataSetInt data)
    {
        super.getResult(data);
        ArrayList<Float> values = new ArrayList<Float>();
        for (NeuronGen<AttributeInt> n : inputNeurons)
        {
            values.add(n.getActivation());
        }
        for (int i = 0; i < hiddenNeuronsLayers.size(); i++)
        {
            values = hiddenLayerActivation(i, values);
        }
        outputActivation(values);
        int pos = 0;
        OutputNeuron<AttributeInt> ret = null;
        for (OutputNeuron<AttributeInt> on : outputNeurons)
        {
            if (pos == 0) ret = on;
            else
            {
                if (ret.getActivation() < on.getActivation()) ret = on;
            }
            pos++;
        }
        return  ret.getContainer();
    }

    /*
    static public MultilayerPerceptron load(String fname)
    {
        Gson gson = new Gson();
        try {
            File                file             = new File(fname);
            FileReader          fileReader       = new FileReader(file);
            BufferedReader      bufferedReader   = new BufferedReader(fileReader);
            return gson.fromJson(bufferedReader.readLine(), MultilayerPerceptron.class);
        }
        catch (IOException e) { System.out.println(e);}
        return null;
    }*/

    public static void main(String[] args)
    {
        PacmanDataSetNW test = new PacmanDataSetNW( "myData/trainingData.txt");

        DataSetInt tmpdataset = test.getDataSet().getClassTypes().getTuple(0).removeAttributtes(test.getDataSet().getClassId());
        ArrayList<AttributeInt> input = new ArrayList<AttributeInt>();
        for (String id: tmpdataset.getAttributeNames())
        {
            input.add(tmpdataset.getValue(id,0));
        }

        ArrayList<Integer> hiddens = new ArrayList<Integer>();
        //hiddens.add(50);
        hiddens.add(10);

        ArrayList<AttributeInt> output = test.getDataSet().getClassTypes().getAttributeArray(test.getDataSet().getClassTypes().getClassId());

        MultilayerPerceptron mlp = new MultilayerPerceptron(input, hiddens, output);

        mlp.train(test.getDataSet(), 75, 1000);

        //mlp.serialize("defaultnetwork.mpobj");
        //mlp.serializeLayers("testlayers.mplyrs");

        //MultilayerPerceptron mlp2 = new MultilayerPerceptron(input, hiddens, output);
        //mlp2.loadLayers("testlayers.mplyrs");

        System.out.println("Testing end");
    }
}
