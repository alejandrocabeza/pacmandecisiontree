package neuralNetwork;

import java.util.ArrayList;

/**
 * Implements the functionality an output neuron uses
 * Type corresponds to the type the neuron will activate with
 * Created by Strinnityk on 09/12/2014.
 */
public class OutputNeuron<Type> extends NeuronGen<Type>
{
    public OutputNeuron(String id, ArrayList<Float> weights, Type container)
    {
        super(id, weights, container);
    }

    public float calculateError(Type expectedTupleValue)
    {
        float expectedValue   = 0.0f;

        if (expectedTupleValue.equals(container))   expectedValue = 1.0f;
        else                                        expectedValue = 0.0f;

        return (activationValue * (1 - activationValue) * (expectedValue - activationValue));
    }
}
