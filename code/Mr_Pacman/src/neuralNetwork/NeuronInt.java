package neuralNetwork;

import java.util.ArrayList;

/**
 * Defines the basic functionality a neuron uses
 * Created by daniel.sanchez on 09/12/2014.
 */
public interface NeuronInt
{
    /**
     * Calculates the net of the neuron
     * @param values Values of the previous tier of neurons to which this neuron is connected
     * @return Net of the neuron
     */
    float calculateNet(ArrayList<Float> values);

    /**
     * Calculates the activation value of the neuron
     * @param lFactor Instance of LearningFactors, required to use stored static methods
     * @param values Values of the previous tier of neurons to which this neuron is connected
     * @return Activation value of the neuron
     */
    float calculateActivation(LearningFactors lFactor, ArrayList<Float> values);

    /**
     * Neuron activation value getter
     * @return Neuron activation value
     */
    float getActivation();

    /**
     * Calculates the error of the neuron
     * @param errorWeightSummation Summation of the error of the following neurons multiplied by the weight connecting the following neuron to this one
     * @return The error of the neuron
     */
    float calculateError(float errorWeightSummation);

    /**
     * Update wrapper that execute the update of weights and threshold
     * @param learningFactor Learning Factor to be used to vary the weights and threshold
     * @param error Error of the neuron
     * @param values Activation values of the previous tier of neurons
     */
    void update(float learningFactor, float error, ArrayList<Float> values);

    /**
     * Updates the weight of the neuron
     * @param learningFactor Learning Factor to be used to vary the weights and threshold
     * @param error Error of the neuron
     * @param values Activation values of the previous tier of neurons
     */
    void updateWeights(float learningFactor, float error, ArrayList<Float> values);

    /**
     * Updates the threshold of the neuron
     * @param learningFactor Learning Factor to be used to vary the weights and threshold
     * @param error Error of the neuron
     */
    void updateThreshold(float learningFactor, float error);

    /**
     * Neuron weights getter
     * @return Neuron weights
     */
    public ArrayList<Float> getWeights();
}
