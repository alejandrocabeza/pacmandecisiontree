package neuralNetwork;

import java.util.ArrayList;

/**
 * Implements the basic functionality a neuron uses
 * Created by daniel.sanchez on 09/12/2014.
 */
public class NeuronImp implements NeuronInt
{
    /**
     * Weights connecting this neuron to the previous layer
     */
    ArrayList<Float> weights;

    /**
     * Neuron Identification
     */
    String           id;

    /**
     * Neuron activation value
     */
    float            activationValue;

    /**
     * Neuron threshold
     */
    float            threshold;

    /**
     * NeuronImp Constructor
     * @param id Name of the neuron to be created
     * @param weights Neuron weights
     */
    public NeuronImp(String id, ArrayList<Float> weights)
    {
        this.id        = id;
        this.weights   = weights;
        this.threshold = (float)Math.random();
    }

    /**
     * Empty weights NeuronImp Constructor
     * @param id Name of the neuron to be created
     */
    public NeuronImp(String id)
    {
        this.id = id;
        weights = new ArrayList<Float>();
        this.threshold = (float)Math.random();
    }

    /**
     * Randomized weights NeuronImp Constructor
     * @param id Name of the neuron to be created
     * @param weightsSize Amount of weights the neuron needs
     */
    public NeuronImp(String id, int weightsSize)
    {
        this.id = id;
        weights = randomWeights(weightsSize);
        this.threshold = (float)Math.random();
    }

    @Override
    public float calculateNet(ArrayList<Float> values)
    {
        if (values.size() != weights.size()) System.out.println("Size of values and weights not the same.");
        else
        {
            float total = 0.0f;
            for (int i = 0; i < values.size(); ++i) total += (values.get(i) * weights.get(i));
            return (total + threshold);
        }

        return 0.0f;
    }

    @Override
    public float calculateActivation(LearningFactors lFactor, ArrayList<Float> values)
    {
        activationValue = lFactor.calculate(calculateNet(values));
        return activationValue;
    }

    @Override
    public float getActivation() { return activationValue; }

    @Override
    public float calculateError(float errorWeightSummation)
    {
        return activationValue * (1 - activationValue) * errorWeightSummation;
    }

    @Override
    public void update(float learningFactor, float error, ArrayList<Float> values)
    {
        updateWeights(learningFactor, error, values);
        updateThreshold(learningFactor, error);
    }

    @Override
    public void updateWeights(float learningFactor, float error, ArrayList<Float> values)
    {
        float weightInc = 0.0f;

        for (int i = 0; i < weights.size(); ++i)
        {
            weightInc = learningFactor * error * values.get(i);
            weights.set(i, weights.get(i) + weightInc);
        }
    }

    @Override
    public void updateThreshold(float learningFactor, float error)
    {
        float thresholdInc = learningFactor * error;
        threshold          += thresholdInc;
    }

    /**
     * Assigns a randomized weigh ArrayList to the neuron
     * @param size Weights ArrayList size
     */
    public void initRandomWeights(int size)
    {
        weights = randomWeights(size);
    }

    /**
     * Creates a randomized weights ArrayList
     * @param size Weights ArrayList size
     * @return ArrayList containing the weights
     */
    static public ArrayList<Float> randomWeights(int size)
    {
        ArrayList<Float> ret = new ArrayList<Float>();
        for (int i = 0; i < size; ++i) ret.add((float)Math.random());
        //for (int i = 0; i < size; ++i) ret.add(0.5f);
        return ret;
    }

    @Override
    public ArrayList<Float> getWeights()
    {
        return weights;
    }

    public void setWeights(ArrayList<Float> weights)
    {
        this.weights = weights;
    }

    public float getActivationValue() {
        return activationValue;
    }

    public void setActivationValue(float val) {
        this.activationValue = val;
    }
}
