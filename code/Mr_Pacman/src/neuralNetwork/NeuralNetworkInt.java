package neuralNetwork;

import decisionTree.DataSetInt;

/**
 * Defines the basic functionality a Neural Network uses
 * Created by daniel.sanchez on 11/12/2014.
 */
public interface NeuralNetworkInt<Type>
{

    /**
     * Training method with the algorithm of training for neural networks
     * @param data              Dataset for being trained with
     * @param expectedAccuracy  Expected accuracy to stop training when reached
     * @param maxIterations     Max iterations to stop training when reached
     */
    void train      (DataSetInt data, float expectedAccuracy, int maxIterations);

    /**
     * Generic method to get the container value of the highest activated output neuron.
     * @param data Dataset with tuple to be used on the neural network to get the activated output neurons
     * @return A generic type from the container with the highest activated neuron.
     */
    Type getResult  (DataSetInt data);

}
