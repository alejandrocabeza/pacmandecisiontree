package neuralNetwork;

import com.google.gson.Gson;
import decisionTree.AttributeInt;
import decisionTree.DataSetInt;

import java.io.*;
import java.util.ArrayList;

/**
 * Implements the basic functionality a Neural Network uses
 * Created by daniel.sanchez on 11/12/2014.
 */
public class NeuralNetworkImp<Type> implements NeuralNetworkInt<Type>
{
    /**
     * Input neuron layer
     */
    protected ArrayList<NeuronGen>                inputNeurons;

    /**
     * Hidden neuron layer
     */
    protected ArrayList<ArrayList<NeuronInt>>     hiddenNeuronsLayers;

    /**
     * Output neuron layer
     */
    protected ArrayList<OutputNeuron>             outputNeurons;

    /**
     * Learning factor algorith to be used (Mostly SIGMOID)
     */
    protected LearningFactors                     lfactor;

    @Override
    public String toString()
    {
        String ret = "Neural Network: \n";
        for (NeuronGen in: inputNeurons) ret += "\t "+in.getActivation()+"\n";
        for (int i = 0; i < hiddenNeuronsLayers.size(); i++)
            for (NeuronInt hn: hiddenNeuronsLayers.get(i)) ret += "\t\t "+hn.getActivation()+"\n";
        for (OutputNeuron on: outputNeurons) ret += "\t\t\t "+on.getActivation()+"\n";
        return ret;
    }

    /**
     * Neural network implementation constructor
     * @param inputdata         Generic Array list with the values for the input neurons to be initialized
     * @param hiddenLayersInfo  Array list with the number of hidden neurons to be initialized peer layer
     * @param outData           Generic Array list with the values for the output neurons to be initialized
     * @param factor            The learning factor function to be used when leaning SIGMOID by default
     */
    public NeuralNetworkImp(ArrayList<Type> inputdata ,ArrayList<Integer> hiddenLayersInfo, ArrayList<Type> outData, LearningFactors.Factors factor)
    {
        lfactor = new LearningFactors(factor);
        inputNeurons        = new ArrayList<NeuronGen>();
        for (int i = 0; i < inputdata.size(); i++)
        {
            Type e = inputdata.get(i);
            inputNeurons.add(new InputNeuron<Type>("InputN_"+i,e));
        }
        hiddenNeuronsLayers = new ArrayList<ArrayList<NeuronInt>>();
        for (int i = 0; i < hiddenLayersInfo.size(); i++)
        {
            hiddenNeuronsLayers.add(new ArrayList<NeuronInt>());
            int weightsSize;
            if (i == 0) weightsSize = inputNeurons.size();
            else        weightsSize = hiddenLayersInfo.get(i-1);
            for (int j = 0; j < hiddenLayersInfo.get(i); j++)
            {
                hiddenNeuronsLayers.get(i).add(new HiddenNeuron("HiddenN_"+i+"_"+j, weightsSize));
            }
        }
        outputNeurons       = new ArrayList<OutputNeuron>();
        for (int i = 0; i < outData.size(); i++)
        {
            Type e = outData.get(i);
            outputNeurons.add(new OutputNeuron<Type>( "OutN_"+i,
                                                      NeuronImp.randomWeights(hiddenLayersInfo.get(hiddenLayersInfo.size() - 1)),
                                                      e));
        }

    }

    /**
     * Initializes the input neurons with a dataset
     * @param data DataSet to initialize the neurons with
     */
    protected void initializeInputNeurons(DataSetInt data)
    {
        for (NeuronGen<AttributeInt> in : inputNeurons)
        {
            //System.out.println(data.getValue(in.getContainer().getId(),0));
            in.setContainer(data.getValue(in.getContainer().getId(),0));
            in.setActivationValue((Float)in.getContainer().getValue());
        }

    }

    @Override
    public void train(DataSetInt data, float expectedAccuracy, int maxIterations)
    {

    }

    /**
     * One step training of the neural network
     * @param data          Dataset with just one tuple to be trained with.
     * @param iteration     Learning factor. Recommended using: 1/iteration
     */
    protected void trainOne(DataSetInt data, int iteration)
    {

    }

    /**
     * Error backpropagation algorithm
     * It's used when the output is not the expected
     * @param data              Dataset with just one tuple in which the expected value is stored in the classId column
     * @param iterationValue    Learning factor. Recommended using: 1/iteration
     */
    protected void backpropagateError(DataSetInt data, float iterationValue)
    {

    }


    /**
     * Error backpropagation algorithm
     * It's used when the output is not the expected
     * @param data              Dataset with just one tuple in which the expected value is stored in the classId column
     */
    /*
    @Deprecated
    protected void backPropagateError (DataSetInt data)
    {
        String id   = data.getAttributeNames().get(data.getAttributeNames().size() - 1);
        Type cls    = (Type)data.getValue(id, 0);
    }*/

    @Override
    public Type getResult(DataSetInt data)
    {
        initializeInputNeurons(data);
        return null;
    }

    /* Tested, not needed...
    protected ArrayList<Float> hiddenLayersNet(int layer, ArrayList<Float> values)
    {
        ArrayList<Float> ret = new ArrayList<Float>();
        for (NeuronInt n : hiddenNeuronsLayers.get(layer)) ret.add(n.calculateNet(values));
        return ret;
    }

    protected void outputNet(ArrayList<Float> values)
    {
        for (NeuronImp n: outputNeurons)
        {
            n.setActivationValue(n.calculateNet(values));
        }
    }
    */

    /**
     * Gets an array list with the activation of the asked hidden layer for the given values
     * @param layer  Hidden Layer to get the activation values from
     * @param values Activation values from the previous layer to the one you want to get the activation values from
     * @return Array list with the activation of the asked layer for the given values
     */
    protected ArrayList<Float> hiddenLayerActivation(int layer, ArrayList<Float> values)
    {
        ArrayList<Float> ret = new ArrayList<Float>();
        for (NeuronInt n : hiddenNeuronsLayers.get(layer)) ret.add(n.calculateActivation(lfactor, values));
        return ret;
    }

    /**
     * Gets the activation values from the selected layer
     * @param layer Layer to get the values from.
     * @return Array list with the activation of the asked layer
     */
    protected ArrayList<Float> hiddenLayerActivation(int layer)
    {
        ArrayList<Float> ret = new ArrayList<Float>();
        for (NeuronInt n : hiddenNeuronsLayers.get(layer)) ret.add(n.getActivation());
        return ret;
    }

    /**
     * Calculates the activation values for the output layer
     * @param values Values from the last hidden layer to be calculate with.
     */
    protected void outputActivation(ArrayList<Float> values)
    {
        for (NeuronImp n: outputNeurons)
        {
            n.setActivationValue(n.calculateActivation(lfactor, values));
        }
    }

    /*
    protected void serializeLayers(String fname)
    {
        Gson gson = new Gson();
        ArrayList<String> tosave = new ArrayList<String>();
        tosave.add(gson.toJson(inputNeurons));
        tosave.add(gson.toJson(hiddenNeuronsLayers));
        tosave.add(gson.toJson(outputNeurons));

        try {
            PrintWriter writer = new PrintWriter(fname);
            for (String obj: tosave) writer.println(obj);
            writer.close();
        }
        catch (IOException e){ System.out.println(e);}
    }

    public void loadLayers(String fname)
    {
        Gson gson = new Gson();
        try {
            File            file             = new File(fname);
            FileReader      fileReader       = new FileReader(file);
            BufferedReader  bufferedReader   = new BufferedReader(fileReader);

            inputNeurons        = gson.fromJson(bufferedReader.readLine(),         inputNeurons.getClass());
            hiddenNeuronsLayers = gson.fromJson(bufferedReader.readLine(),  hiddenNeuronsLayers.getClass());
            outputNeurons       = gson.fromJson(bufferedReader.readLine(),        outputNeurons.getClass());
        }
        catch (IOException e) { System.out.println(e);}
    }

    public void serialize(String fname)
    {
        Gson gson = new Gson();
        try {
            PrintWriter writer = new PrintWriter(fname);
            writer.println(gson.toJson(this));
            writer.close();
        }
        catch (IOException e){ System.out.println(e);}
    }

    static public NeuralNetworkImp load(String fname)
    {
        Gson gson = new Gson();
        try {
            File            file             = new File(fname);
            FileReader      fileReader       = new FileReader(file);
            BufferedReader  bufferedReader   = new BufferedReader(fileReader);
            return gson.fromJson(bufferedReader.readLine(), NeuralNetworkImp.class);
        }
        catch (IOException e) { System.out.println(e);}
        return null;
    }
    */
}
