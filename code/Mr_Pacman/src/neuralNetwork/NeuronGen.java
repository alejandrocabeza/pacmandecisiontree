package neuralNetwork;

import java.util.ArrayList;

/**
 * Neuron that contains a generic container
 * Created by daniel.sanchez on 16/12/2014.
 */
public class NeuronGen<Type> extends NeuronImp
{
    /**
     * Generic variable
     * Type stands for the type of the datum
     */
    Type container;

    /**
     * NeuronGen Constructor
     * @param id Name of the neuron to be created
     * @param weights Neuron weights
     * @param container Value to be stored. Type is the type.
     */
    public NeuronGen(String id, ArrayList<Float> weights, Type container)
    {
        super(id, weights);
        this.container = container;
    }

    Type getContainer(){return container;}
    void setContainer(Type newcontainer){container = newcontainer;}
}
