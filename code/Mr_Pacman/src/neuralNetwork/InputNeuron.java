package neuralNetwork;

import java.util.ArrayList;

/**
 * Implements the functionality an input neuron uses
 * Created by Strinnityk on 09/12/2014.
 */
public class InputNeuron<Type> extends NeuronGen<Type>
{
    public InputNeuron(String id, Type container)
    {
        super(id ,new ArrayList<Float>(), container);
    }
}
